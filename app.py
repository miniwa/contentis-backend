#!/usr/bin/env python3
# -*- coding: utf-8 -*-
ver = "0.1"

#flask imports
from flask import Flask
from flask import abort
from flask import jsonify
from flask import make_response
from flask import request
from flask import send_from_directory

#general imports
import sys
import json
import uuid

#scraper import
import requests

#image handeling
import base64
from io import BytesIO
from PIL import Image
from resizeimage import resizeimage

adminpasswordvariable = "79l46wgv57b8l45wn9werc"
UPLOAD_FOLDER = '/home/jacwib/mysite/images/'

if sys.version_info[0] < 3:
    raise Exception("you're an idiot")

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False #jsonify otherwise fucks your utf-8
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

items = []

with open('notadb.json') as json_data:
    items = json.load(json_data)
with open('notadbedit.json') as json_data:
    edits = json.load(json_data)

@app.errorhandler(400)
def user_error(error):
    return make_response(jsonify({'error': 'you\'re an idiot'}), 400)

@app.errorhandler(403)
def uh_oh(error):
    return make_response(jsonify({'error': 'magical debug error'}), 402)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.errorhandler(405)
def not_allowed(error):
    return make_response(jsonify({'error': 'fuck off'}), 405)

@app.errorhandler(500)
def oh_shit(error):
    return make_response(jsonify({'error': 'something broke :('}), 500)

@app.route('/api')
def index():
    return ":)<br> ver"+ver

@app.route('/api/'+ver+'/test/<string:number>', methods=['GET'])
def print_some_bullshit(number):
    return number + " ur mom xd"

@app.route('/api/'+ver+'/fetch-item/<string:EAN>', methods=['GET'])
def get_the_infoz(EAN):
    try:
        resp = make_response(jsonify(items[EAN]))
        resp.headers['Content-type'] = "application/json; charset=UTF-8"
        return resp
    except KeyError:
        abort(404)

@app.route('/api/'+ver+'/top-item/')
def get_top_items():
    json = {0: items["7340011454489"], 1: items["07874260"], 2: items["825646890187"]}
    json[0]["ean"] = "7340011454489"
    json[1]["ean"] = "07874260"
    json[2]["ean"] = "825646890187"
    resp = make_response(jsonify(json)) #hardcoded until we have like. a sponsor or something. and even then it'll just be hardcoded, but in a db lmao
    resp.headers['Content-type'] = "application/json; charset=UTF-8"
    return resp

@app.route('/api/'+ver+'/fetch-image/<path:filename>', methods=['GET'])
def fetch_image(filename):
    return send_from_directory('/home/jacwib/mysite/images', filename)

@app.route('/api/'+ver+'/scrape-info/<string:EAN>', methods=['GET'])
def scrape_shit(EAN):
    try:
        headers = {"accept": "application/json"}
        page = requests.get("https://www.coop.se/ws/v2/coop/users/anonymous/products/"+EAN, headers=headers)
        uwu = json.loads(page.content)
        payload = {"ean":uwu["code"],"title":uwu["breadcrumbs"]["breadcrumbs"][0]["name"],"vegan":0,"image-front":"https://res.cloudinary.com/coopsverige/image/upload/d_cooponline:missingimage:missing-image.png,fl_progressive,q_90,c_lpad,w_512,h_512/"+uwu["images"][0]["url"][-11:][:6]+".jpg"}
        resp = make_response(jsonify(payload))
        resp.headers['Content-type'] = "application/json; charset=UTF-8"
        return resp
    except:
        abort(404)

@app.route('/api/'+ver+'/upload-item', methods=['POST'])
def put_the_infoz():
    if not request.json or not 'ean' in request.json or not 'title' in request.json or not 'vegan' in request.json:
        abort(400)
    if 'edit' in request.json and request.json['edit'] == "true":
        edit = 1
    else:
        edit = 0
    if 'image-file' not in request.json:
        try:
            print(jsonify(items[request.json['ean']]))
            if edit == 1:
                raise KeyError
            abort(405)
        except KeyError:
            killme = {
                    'title': request.json['title'],
                    'vegan': request.json['vegan'],
                    'allergies': request.json.get('allergies', {}),
                    'image-front': request.json.get('image-front', ""),
                    'description': '',
                    #'image-back': request.json.get('image-back', "")
            }
            if (edit == "true"):
                edits[request.json['ean']] = killme
            else:
                items[request.json['ean']] = killme
            return jsonify(items)
    else:
        try:
            print(jsonify(items[request.json['ean']]))
            if edit == 1:
                raise KeyError
            abort(405)
        except KeyError:
            try:
                image_data = bytes(request.json['image-file'], encoding="ascii")
                im = Image.open(BytesIO(base64.b64decode(image_data)))
                exif = im.info['exif']
                im = resizeimage.resize_width(im, 512)
            except:
                abort(403)
            filename = str(uuid.uuid4())
            try:
                filenamee = UPLOAD_FOLDER+filename+'.jpg'
                im.save(filenamee, exif=exif)
            except:
                abort(400)
            killme = {
                    'title': request.json['title'],
                    'vegan': request.json['vegan'],
                    'allergies': request.json.get('allergies', {}),
                    'image-front': 'https://jacwib.pythonanywhere.com/api/0.1/fetch-image/'+filename+'.jpg',
                    'description': '',
                    #'image-back': request.json.get('image-back', "")
            }
            try:
                if (edit == "true"):
                    edits[request.json['ean']] = killme
                else:
                    items[request.json['ean']] = killme
                return jsonify(items)
            except:
                abort(500)

@app.route('/api/'+ver+'/admin/save-db/<string:password>', methods=['GET'])
def save_that_shiz(password):
    if password != adminpasswordvariable:
        abort(404) #to make noone suspect theres shit theree
    with open('notadb.json', 'w') as f:
        json.dump(items, f, ensure_ascii=False)
    with open('notadbedit.json', 'w') as ff:
        json.dump(edits, ff, ensure_ascii=False)
    return(jsonify({'success': 'saved'}))

@app.route('/api/'+ver+'/admin/update-item', methods=['POST'])
def put_the_infoz_really():
    if not request.json or not 'password' in request.json or not request.json['password'] == adminpasswordvariable:
        abort(404)
    if not request.json or not 'ean' in request.json or not 'title' in request.json or not 'vegan' in request.json:
        abort(400)
    killme = {
            'title': request.json['title'],
            'vegan': request.json['vegan'],
            'allergies': request.json.get('allergies', {}),
            'image-front': request.json.get('image1', ""),
            'image-back': request.json.get('image2', "")
    }
    print(killme)
    items[request.json['ean']] = killme
    return jsonify(items)

if __name__ == '__main__':
    app.run(debug=True)